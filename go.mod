module bitbucket.org/tinselmann/hugo-bootstrap

go 1.15

require (
	github.com/jquery/jquery-dist v0.0.0-20200504225046-4c0e4becb826 // indirect
	github.com/twbs/bootstrap v4.5.3+incompatible // indirect
)
